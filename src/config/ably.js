const Ably = require('ably');

async function setupAbly() {
    const ably = new Ably.Realtime.Promise('DL-O5Q.Rk-NMQ:0rGf6mV-tKxADi0UwJEBc5qZKmKK7mQQi7KAYgYP4a0');
    await ably.connection.once('connected');
    console.log('Connected to Ably!');
    return ably;
}

module.exports = setupAbly;