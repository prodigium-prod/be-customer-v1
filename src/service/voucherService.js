const bson = require('bson');
// VoucherService.js

const claimAllMembership = (voucher, user) => {
    if (voucher.maxClaims <= voucher.claimed) {
        return "Sorry, the maximum claims for this voucher have been reached.";
    }

    // Create a newVoucher object with the necessary voucher details
    const newVoucher = {
        id : new bson.ObjectId(),
        zone : voucher.zone,
        code: voucher.code,
        discount: voucher.discount,
        type: voucher.type,
        membership: voucher.membership,
        event: voucher.event,
        transMinimum: voucher.transMinimum,
        maxDiscount: voucher.maxDiscount,
        maxClaims: voucher.maxClaims,
        claimed: voucher.claimed + 1,
        validFrom: voucher.validFrom,
        validUntil: voucher.validUntil,
        maxUserUsed: voucher.maxUserUsed,
        voucherUsed: voucher.voucherUsed,
        tnc: voucher.tnc,
        image: voucher.image,
    };

    user.vouchers.push(newVoucher);
    user.save()
    voucher.claimed += 1;
    voucher.save();
    return "Congrats!! You have a new voucher.";
};

const claimMembership = (voucher, user, membershipType) => {
    if (voucher.membership === membershipType) {
        if (voucher.maxClaims <= voucher.claimed) {
            return "Sorry, the maximum claims for this voucher have been reached.";
        }
        // Create a newVoucher object with the necessary voucher details
        const newVoucher = {
            id : new bson.ObjectId(),
            zone : voucher.zone,
            code: voucher.code,
            discount: voucher.discount,
            type: voucher.type,
            membership: voucher.membership,
            event: voucher.event,
            transMinimum: voucher.transMinimum,
            maxDiscount: voucher.maxDiscount,
            maxClaims: voucher.maxClaims,
            claimed: voucher.claimed + 1,
            validFrom: voucher.validFrom,
            validUntil: voucher.validUntil,
            maxUserUsed: voucher.maxUserUsed,
            voucherUsed: voucher.voucherUsed,
            tnc: voucher.tnc,
            image: voucher.image,
        };

        user.vouchers.push(newVoucher);
        user.save()
        voucher.claimed += 1;
        voucher.save();
        return "Congrats!! You have a new voucher.";
    } else {
        return `You can't claim this voucher, it's only available for ${voucher.membership} members.`;
    }
};

claimVoucher = (voucher, user, membershipType) => {
    const currentDate = new Date();
    const formattedDateNow = `${currentDate.getFullYear()}-${String(currentDate.getMonth() + 1).padStart(2, '0')}-${String(currentDate.getDate()).padStart(2, '0')}`;
    let message = "";

    if (!voucher) {
        return "Voucher not found.";
    }

    if (voucher.canBeClaimed !== 1) {
        return "Voucher can't be claimed.";
    }

    if (!(voucher.claimFrom < formattedDateNow && voucher.claimUntil > formattedDateNow)) {
        return "Voucher is not within the claimable period.";
    }

    switch (voucher.membership) {
        case "All":
            return claimAllMembership(voucher, user);

        case "Fresh":
            return claimMembership(voucher, user, membershipType);

        case "Sparkle":
            return claimMembership(voucher, user, membershipType);

        case "Crystal":
            return claimMembership(voucher, user, membershipType);

        case "Shimmering":
            return claimMembership(voucher, user, membershipType);

        case "Splendid":
            return claimMembership(voucher, user, membershipType);

        default:
            return "Voucher can't be claimed for the specified membership.";
    }
};

newUserVoucher = (voucher, user) => {

    const newVoucher = {
        id : new bson.ObjectId(),
        zone : voucher.zone,
        code: voucher.code,
        discount: voucher.discount,
        type: voucher.type,
        membership: voucher.membership,
        event: voucher.event,
        transMinimum: voucher.transMinimum,
        maxDiscount: voucher.maxDiscount,
        maxClaims: voucher.maxClaims,
        claimed: voucher.claimed,
        validFrom: voucher.validFrom,
        validUntil: voucher.validUntil,
        maxUserUsed: voucher.maxUserUsed,
        voucherUsed: voucher.voucherUsed,
        tnc: voucher.tnc,
        image: voucher.image,
    };

    user.vouchers.push(newVoucher);
    user.save()
    voucher.claimed += 1;
    voucher.save();

};

module.exports = {claimVoucher, newUserVoucher};