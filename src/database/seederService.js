const mongoose = require('mongoose');
const ServiceModel = require('../model/mongodb/service_mongose');

const connectDB = async () => {
    try {
        await mongoose.connect('mongodb+srv://kilapin:Ifvu1ovfqbhXFTRE@cluster0.oucnaua.mongodb.net/kilapin-dev', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log('MongoDB connected...');
        seedDB();
    } catch (err) {
        console.log(err);
    }
}
const descriptions = ['15 - 35 m2', '36 - 70 m2', '71 - 135 m2', '136 - 200 m2', '201 - 250 m2', '251 - 300 m2', '301 - 400 m2', '401 - 500 m2'];
const prices = [100000, 150000, 200000, 250000, 300000, 350000, 400000, 450000];
const options = ['Granite', 'Marmer', 'Nothing','Granite and Marmer'];
const optionPriceAdjustments = [10000, 8000, 0, 18000]; // Ini adalah penyesuaian harga untuk masing-masing pilihan

let services = [];
for (let i = 0; i < descriptions.length; i++) {
    for (let j = 0; j < options.length; j++) {
            const service = {
                name: `Apartment`,
                description: descriptions[i],
                option: options[j],
                price: prices[i] + optionPriceAdjustments[j]
            };
            services.push(service);
    }
}

const seedDB = async () => {
    try {
        await ServiceModel.deleteMany({});
        await ServiceModel.create(services);
        console.log('Data seeding berhasil!');
        mongoose.connection.close();
    } catch (error) {
        console.log("Error:", error);
    }
}

connectDB();