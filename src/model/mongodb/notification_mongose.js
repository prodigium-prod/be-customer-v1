const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
    order_id: {type: String,default: null},
    fraud_status: {type: String, default: null},
    transaction_status: {type: String,default: null}
}, {timestamps: true})

const Notification = mongoose.model('Notification',notificationSchema)

module.exports = Notification;