const mongoose = require('mongoose');

const voucherSchema = new mongoose.Schema({
    type: {
        type: String,
        required: true
    },
    zone: {
        type: String,
        default: null
    },
    service: {
        type: String,
        required: true
    },
    code: {
        type: String,
        unique: true,
        required: true
    },
    membership: {
        type: String,
        required: true
    },
    event: {
        type: String,
        required: true
    },
    transMinimum: {
        type: Number,
        required: true
    },
    discount: {
        type: Number,
        required: true
    },
    maxDiscount: {
        type: Number,
        default: 0
    },
    maxClaims: {
        type: Number,
        required: true
    },
    claimed: {
        type: Number,
        default: 0
    },
    validFrom: {
        type: String,
        required: true
    },
    validUntil: {
        type: String,
        required: true
    },
    claimFrom: {
        type: String,
        default: null,
    },
    claimUntil: {
        type: String,
        default: null,
    },
    canBeClaimed: {
        type: Number,
    },
    maxUserUsed: {
        type: Number,
        default: 0
    },
    voucherUsed: {
        type: Number,
        default: 0
    },
    tnc: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },

},{timestamps: true});

const Voucher = mongoose.model('Voucher', voucherSchema);

module.exports = Voucher
