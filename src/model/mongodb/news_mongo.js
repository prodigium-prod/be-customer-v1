const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const newsSchema = new Schema({
    title: {type: String, default: null},
    desc: {type: String, default: null},
    image: {type: String, default: null},
    status: {type: String, default: null},
}, {timestamps: true});


const News = mongoose.model("News", newsSchema);
module.exports = News