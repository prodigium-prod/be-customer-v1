const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const orderSchema = new mongoose.Schema({
    order_id: {type: String},
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    service_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Service'},
    category: { type: String, required: true },
    booking_type: { type: String, required: true },
    address: {type: String, default: null },
    insurance: {type: String, default: null },
    time: {type: String, default: null },
    cleaner_id: { type: String, default: null },
    status: { type: String,default: 'Waiting for Payment', required: true },
    initial_price: { type: Number, default: null  },
    total_price: { type: Number, default: null  },
    total_discount: { type: Number, default: 0  },
    cleaner_balance: { type: Number, default: 0  },
    voucher: {type:String, default: null },
    notes: { type: String, default: null  },
    rating: { type: Number, default: null  },
    review: { type: String, default: null  },
    status_payment: {type: String, default: null},
    postal_code: {type: Number, default:null},
    cleaner_status: {type: String, default: null},
    lng: {type: Number},
    lat: {type: Number},
    additional_service :[mongoose.Schema.Types.Mixed],
    payment_url : {type: String, default: null},
}, {timestamps: true});

const Order = mongoose.model('Order', orderSchema);


module.exports = Order;