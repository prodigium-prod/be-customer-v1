const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const cleanerDetailSchema = new mongoose.Schema({
    _id : {type: mongoose.Schema.Types.ObjectId},
    review_status: {type: String, default: null},
    rating: {type: Number, default: null},
    domisili: {type: String, default: null},
    nik: {type: String, default: null},
    photo_ktp: {type: String, default: null},
    no_npwp: {type: String, default: null},
    foto_npwp: {type: String, default: null},
    foto_ijazah: {type: String, default: null},
    education: {type: String, default: null},
    bank: {type: String, default: null},
    no_bank: {type: String, default: null},
    no_sertif_bnsp: {type: String, default: null},
    sertif_bnsp: {type: String, default: null},
    no_sim_c : {type: String, default: null},
    foto_sim_c : {type: String, default: null},
    status: {type: String, default: null},
}, {timestamps: true});

const CleanerDetail = mongoose.model('CleanerDetail', cleanerDetailSchema);

module.exports = CleanerDetail;