const {response} = require("../middleware/common");
const bcrypt = require('bcryptjs');
const axios = require('axios');
const user_mongo = require('../model/mongodb/user_mongo')
const voucher_mongo = require("../model/mongodb/voucher_mongose");
const {newUserVoucher} = require("../service/voucherService");


const UsersController = {
    register: async (req, res, next) => {
        const user = await user_mongo.findOne({ $or: [{ email: req.body.email }, { phone: req.body.phone }] });
        if (user){
            return response(res, 404, false, null, "you already registered")
        }
        // let acak = ""
        let digits = "0123456789";
        let otp = "";
        for (let i = 0; i < 6; i++) {
            otp += digits[Math.floor(Math.random() * 10)];
        }

        function makeid(length) {
            let result = '';
            const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            const charactersLength = characters.length;
            let counter = 0;
            while (counter < length) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
                counter += 1;
            }
            return result;
        }

        const password1 = req.body.password
        const password2 = req.body.confirm
        if (password1 !== password2) {
            return response(res, 404, false, null, "password not matched")
        }
        try {
            const hashedPassword = bcrypt.hashSync(req.body.password);
            const newUser = new user_mongo({
                name: req.body.name,
                email: req.body.email,
                password: hashedPassword,
                phone: req.body.phone,
                address: req.body.address,
                postal_code: req.body.postal_code,
                role: 'Kilapin Customer',
                member: 'Fresh',
                referral_code: makeid(5)
            })
            const result = await newUser.save()
            const voucher = await voucher_mongo.findOne({code: "Pengguna Baru"});
            newUserVoucher(voucher, result);
            if (result) {
                response(res, 200, true, result, "register success")
            }
        } catch (err) {
            console.log(err)
            response(res, 404, false, err, "register fail")
        }
    }, login_mongo: async (req, res, next) => {
        try {
            const user = await user_mongo.findOne({email: req.body.email});
            if (user === null) {
                response(res, 404, true, user, "Email tidak ditemukan")
            }
            const password = req.body.password
            const validation1 = bcrypt.compareSync(password, user.password)
            if (!validation1) {
                return response(res, 404, false, null, "wrong password")
            }
            if (user.auth === 0) {
                return response(res, 404, false, user, "Account Belum Di-verifikasi")
            }
            response(res, 200, true, user, "login success")
        } catch (error) {
            console.error(error);
            response(res, 404, false, error, "login fail")
        }
    }, verificationWeb: async (req, res, next) => {
        try {
            const result = await user_mongo.updateOne({phone: req.params.phone}, {$set: {auth: 1}})
            const user = await user_mongo.findOne({phone: req.params.phone});
            if (result) {
                response(res, 200, true, user, "verification account success")
            }
        } catch (error) {
            response(res, 404, false, error, " user not found")
        }
    }, getProfileDetail: async (req, res, next) => {
        try {
            const user = await user_mongo.findOne({_id: req.params.id});
            response(res, 200, true, user, "get profile success")
        } catch (error) {
            console.log(error)
            response(res, 404, false, error, "get profile fail")
        }

    }, changePass: async (req, res, next) => {
        try {
            const user = await user_mongo.findOne({phone: req.params.phone});
            const result = await user_mongo.updateOne({phone: req.params.phone}, {$set: {password: bcrypt.hashSync(req.body.password)}})
            if (user) {
                response(res, 200, true, user, "Change Password Success")
            } else {
                response(res, 404, false, user, "Tidak Ada Akun dengan Nomor Tersebut")
            }
        } catch (error) {
            console.log(error)
            response(res, 404, false, error, "Ini Error")
        }
    }, checkPhone: async (req, res, next) => {
        try {
            const user = await user_mongo.findOne({phone: req.params.phone});

            if (!user) {
                return response(res, 404, false, user, "Tidak Ada Akun dengan Nomor Tersebut");
            }

            const phone = req.params.phone;
            const data = {
                content: ['Kilapin Apps'], msisdn: `62${phone}`, lang_code: 'en', time_limit: '300'
            };
            const headers = {
                accept: 'application/json',
                'App-ID': 'b2d6dc1a-e725-4063-b764-1821de8d623e',
                'API-Key': 'VGE74784/xZP9hENjU18ifDY0mLvuMuW',
                'content-type': 'application/json'
            };

            const axiosResponse = await axios.post('https://api.verihubs.com/v1/whatsapp/otp/send', data, {headers});

            return res.json(axiosResponse.data);
        } catch (error) {
            return response(res, 500, false, error, "Ini Error");
        }
    }, checkOTP: async (req, res, next) => {
        try {
            const data = {
                msisdn: `62${req.params.phone}`, otp: req.params.otp
            }
            const headers = {
                accept: 'application/json',
                'App-ID': 'b2d6dc1a-e725-4063-b764-1821de8d623e',
                'API-Key': 'VGE74784/xZP9hENjU18ifDY0mLvuMuW',
                'content-type': 'application/json'
            };
            const result = await axios.post('https://api.verihubs.com/v1/whatsapp/otp/verify', data, {headers})
            // response(res,200,true,result,"result OKE")
            res.json(result.data);
            // const result = await 
        } catch (error) {
            response(res, 404, false, error, "ini error guys")
        }
    }
}

exports.UsersController = UsersController;