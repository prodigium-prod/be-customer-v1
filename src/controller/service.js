const {response} = require("../middleware/common");
const ModelService = require("../model/mongodb/service_mongose")
const AdditionalService = require('../model/mongodb/additional_service_mongo')

const serviceController = {
    inputService: async (req, res, next) => {
        try {
            const data = new ModelService({
                name: req.body.name,
                description: req.body.description,
                option: req.body.option,
                insurance: req.body.insurance,
                price: req.body.price
            })
            const result = await data.save()
            if (result) {
                response(res, 200, true, data, "Input Service Success")
            }
        } catch (error) {
            response(res, 400, false, data, "Input Service Failed")
        }
    }, searchService: async (req, res, next) => {
        try {
            const data = await ModelService.find({description: req.body.description, option: req.body.option})
            if (data) {
                response(res, 200, true, data, "Search Service Success")
            }
        } catch (error) {
            response(res, 400, false, error, "Search Service Failed")
        }
    }, getAllService: async (req, res, next) => {
        try {
            const data = await ModelService.find()
            if (data) {
                response(res, 200, true, data, "Get All Service Success")
            }
        } catch (error) {
            response(res, 400, false, error, "Get All Service Failed")
        }
    }, getAllAdditional : async (req,res,next) => {
        try{
            const filter = {
                category : req.params.category
            }
            const data = await AdditionalService.find(filter)
            response(res, 200, true, data, "Get All Additional Service Success")
        }catch (e) {
            response(res, 400, false, e, "Get All Additional Service Failed")
        }
    }
}

exports.serviceController = serviceController