const {response} = require("../middleware/common");
const voucher_mongo = require('../model/mongodb/voucher_mongose')
const user_mongo = require("../model/mongodb/user_mongo");
const {claimVoucher} = require("../service/voucherService");


const VoucherController = {
        claimVoucherController: async (req, res, next) => {
            try {
                const user = await user_mongo.findOne({ _id: req.body.user_id });
                const codeVoucher = req.body.code_voucher;
                const voucher = await voucher_mongo.findOne({ code: codeVoucher });
                const membershipType = req.body.membership

                const message = claimVoucher(voucher, user, membershipType);
                response(res, 200, true, user, message);
            } catch (error) {
                response(res, 500, false, null, "Error while processing voucher claim.");
            }
        },
}
exports.VoucherController = VoucherController