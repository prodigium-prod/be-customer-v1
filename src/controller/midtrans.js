const {response} = require("../middleware/common");
const OrderMongo = require("../model/mongodb/order_mongose")
const NotificationMongo = require("../model/mongodb/notification_mongose")
const axios = require('axios')
const pusher = require("../config/pusher");

const MidtransController = {
    insert: async (req, res, next) => {
        const data = new NotificationMongo({
            order_id: req.body.order_id,
            fraud_status: req.body.fraud_status,
            transaction_status: req.body.transaction_status
        })
        try {
            const result = await data.save()
            console.log(data)
            response(res, 200, true, result, "payment success")
        } catch (error) {
            console.log(error)
            response(res, 404, false, error, "please pay your order")
        }
    }, status: async (req, res) => {
        const orderId = req.params.order_id;
        try {
            const notifications = await NotificationMongo.find({order_id: orderId}).lean();

            if (!notifications.length) {
                return response(res, 200, false, null, "Silahkan mencoba membayar");
            }

            const order = await OrderMongo.findOne({order_id: orderId}).lean();
            const latestNotification = notifications[notifications.length - 1];
            console.log(latestNotification)
            const userId = order.user_id

            let newStatus = order.status;
            let newPayment = order.status_payment;
            let message;

            switch (latestNotification.transaction_status) {
                case "settlement":
                    newPayment = "Paid";
                    if (order.booking_type === "Booking") {
                        newStatus = "Ready to Booked";
                        console.log("tahap booking driver");
                    } else {
                        newStatus = "Open";
                        console.log("tahap open driver");
                        axios.post("https://cleaner.kilapin.com/order/finding-cleaner", { order_id: orderId });
                        message = "Sudah membayar";
                        break;
                    }
                    message = "Sudah membayar";
                    break;

                case "expire":
                    newStatus = "Cancel, Expired Payment";
                    message = "Pembayaran expired";
                    break;

                case "pending":
                    message = "Silahkan dibayar";
                    break;

                default:
                    message = "Silahkan mencoba membayar";
            }

            await OrderMongo.updateOne({order_id: orderId}, {status: newStatus, status_payment: newPayment});

            response(res, 200, true, {notifications, newStatus, newPayment}, message);

        } catch (error) {
            response(res, 404, false, error, "Tidak ada percobaan pembayaran");
        }
    }
}

exports.MidtransController = MidtransController