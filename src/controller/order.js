const {response} = require("../middleware/common");
const midtransClient = require('midtrans-client');
const OrderMongo = require("../model/mongodb/order_mongose")
const UserMongo = require("../model/mongodb/user_mongo")
const ServiceMongo = require("../model/mongodb/service_mongose")
const NotificationMongo = require("../model/mongodb/notification_mongose")
const pusher = require("../config/pusher");
const voucher_mongo = require('../model/mongodb/voucher_mongose')
const CleanerDetail = require('../model/mongodb/CleanerDetail')


let snap = new midtransClient.Snap({
    isProduction: true, serverKey: 'Mid-server-WN5ll-9sz_kBNQGUZ2CWDxre', clientKey: 'Mid-client-3C5zJW2fxujlXS5L'
    // isProduction: false, serverKey: 'SB-Mid-server-cHiJyn4sJ6SaZ4OhlnDaUaoL', clientKey: 'SB-Mid-client-5zQqBEzT5-ap3oZw'
});

const OrderController = {
    insert: async (req, res, next) => {

        function generateOrderId() {
            const timestamp = new Date().getTime();
            const randomString = "ORDER";
            return `${randomString}-${timestamp}`;
        }

        const order_id = generateOrderId();
        const gross_amount = req.body.gross_amount
        const address = req.body.address
        const customer_id = req.body.customer_id
        const customer = await UserMongo.findOne({_id: customer_id});
        const service_id = req.body.service_id
        const serviceData = await ServiceMongo.findOne({_id: service_id});
        const category = req.body.category
        const additionalServiceArray = req.body.additional_service || [];

        const data_mongo = new OrderMongo({
            order_id: order_id,
            user_id: customer_id,
            service_id: req.body.service_id,
            category: req.body.category,
            booking_type: req.body.booking_type,
            address: address,
            time: req.body.time,
            insurance: req.body.insurance,
            total_price: gross_amount,
            initial_price: req.body.initial_price,
            total_discount: req.body.total_discount,
            voucher: req.body.voucher,
            notes: req.body.notes,
            status_payment: 'Unpaid',
            postal_code: req.body.postal_code,
            cleaner_status: 'Finding',
            lng: req.body.longitude,
            lat: req.body.latitude,
            additional_service: req.body.additional_service,
            cleaner_balance: req.body.cleaner_balance,
        })
        const result_mongo = await data_mongo.save()
        let voucher;
        if(req.body.voucher){
            voucher = await voucher_mongo.findOne({code : req.body.voucher})
        }
        const parameter = {
            "transaction_details": {
                "order_id": `${order_id}`, "gross_amount": `${gross_amount}`
            }, "item_details": [{
                "name": `${category} - ${serviceData.option} ${serviceData.description}`,
                "price": `${serviceData.price}`,
                "quantity": 1
            },
                ...additionalServiceArray.map(service => ({
                    "name": `${service.type} ${service.type_option} ${service.type_class}`,
                    "price": service.price,
                    "quantity": service.total
                })),
                ...(voucher ? [
                    {
                        "name": `Voucher ${voucher.code}`,
                        "price": -req.body.total_discount,
                        "quantity": 1
                    }
                ] : []),
                ...(req.body.insurance ? [
                    {
                        "name": "Insurance",
                        "price": 5000,
                        "quantity": 1
                    }
                ] : []),
            ], "customer_details": {
                "first_name": `${customer.name}`,
                "email": `${customer.email}`,
                "phone": `${customer.phone}`,
                "billing_address": {
                    "address": `${customer.address}`
                }
            }, // "enabled_payments": ["gopay","permata_va"],
            // "callbacks": {
            //     "finish": link
            // },
            "page_expiry": {
                "duration": 30, "unit": "minutes"
            }
        }
        try {
            snap.createTransaction(parameter)
                .then((transaction) => {
                    // transaction token
                    let transactionToken = transaction.token;
                    console.log('transactionToken:', transactionToken);
                    // transaction redirect url
                    const transactionRedirectUrl = transaction.redirect_url;

                    console.log('transactionRedirectUrl:', transactionRedirectUrl);
                    result_mongo.payment_url = transactionRedirectUrl
                    result_mongo.save();
                    response(res, 200, true, result_mongo, `${transactionRedirectUrl}`)
                })
                .catch((e) => {
                    console.log('Error occured:', e.message);
                });
        } catch (error) {
            console.log(error)
            response(res, 404, false, error, "order fail")
        }
    }, orderDetail: async (req, res, next) => {

        try {
            const order = await OrderMongo.findOne({order_id: req.params.order_id})
                .populate("cleaner_id")
                .populate("service_id")
            console.log(order)

            let result = {}; // Declare the result variable outside the if-else block

            if (order.cleaner_id) {
                const cleaner = await UserMongo.findOne({_id: order.cleaner_id});
                console.log(cleaner);

                result = {
                    order: order, cleaner: cleaner,
                };
            } else {
                result = {
                    order: order,
                };
            }

            response(res, 200, true, result, "get order success")
        } catch (error) {
            response(res, 404, false, error, "get order fail")
        }
    }, getOngoing: async (req, res, next) => {
        try {
            const orders = await OrderMongo.find({
                user_id: req.params.customer_id, status_payment: "Paid", status: {$ne: "Done"},
            }).populate("service_id")
                .sort({createdAt: -1});
            if (orders) {
                // await pusher.trigger('my-channel', `${req.params.customer_id}`, orders);
                response(res, 200, true, orders, "get order success")
            }
        } catch (error) {
            response(res, 404, false, error, "get order fail")
        }
    }, Review: async (req, res, next) => {
        try {
            const result = await OrderMongo.updateOne({order_id: req.params.order_id}, {
                $set: {
                    review: (req.body.review), rating: (req.body.rating)
                }
            })
            if (result) {
                const order = await OrderMongo.findOne({order_id: req.params.order_id});
                const data = {
                    order_id: order.order_id, review: order.review, rating: order.rating
                }
                response(res, 200, true, data, "Input Review Success")
            } else {
                response(res, 404, false, null, "Tidak Ada Order Tersebut")
            }
        } catch (error) {
            response(res, 404, false, error, "order cancel fail")
        }
    }, orderDetailTracking: async (req, res, next) => {
        try {
            const result = await OrderMongo.findOne({order_id: req.params.id})
                .populate("cleaner_id")
                .populate("service_id")
            console.log(result.cleaner_id)
            response(res, 200, true, result, "get order success")
        } catch (error) {
            response(res, 404, false, error, "get order fail")
        }
    }, putStatus: async (req, res, next) => {
        try {
            if (req.params.status === "Done") {
                const order = await OrderMongo.findOne({order_id: req.params.order_id})
                const cleaner_balance = order.initial_price * 0.6;
                await OrderMongo.updateOne({order_id: req.params.order_id}, {$set: {cleaner_balance: cleaner_balance}});
                const cleaner = await UserMongo.findOne({_id: order.cleaner_id}).populate("cleaner_detail")
                cleaner.cleaner_detail.status = "Ready";
                cleaner.cleaner_detail.save();
            }
            const result = await OrderMongo.updateOne({order_id: req.params.order_id}, {$set: {status: (req.params.status)}})
            if (result) {
                const data = {
                    order_id: result.order_id, status: result.status
                }
                response(res, 200, true, result, "Change Status Success")
            } else {
                response(res, 404, false, result, "Tidak Ada Akun dengan Nomor Tersebut")
            }
        } catch (error) {
            console.log(error)
            response(res, 404, false, error, "Ini Error")
        }
    }
}

exports.OrderController = OrderController;