const {response} = require("../middleware/common");
const News_Mongose = require("../model/mongodb/news_mongo")

const NewsController = {

     getAll: async (req, res, next) => {
        try {
            const result = await News_Mongose.find({status: 'Active'})
            response(res, 200, true, result, "get all news success")
        } catch (error) {
            response(res, 404, false, error, "get all news failed")
        }
    }, getDetail: async (req, res, next) => {
        try {
            const result = await News_Mongose.findOne({_id: req.params.id})
            response(res, 200, true, result, "get detail news success")
        } catch (error) {
            response(res, 404, false, error, "get detail news failed")
        }
     }
}

exports.NewsController = NewsController