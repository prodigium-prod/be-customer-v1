const express = require('express')
const router = express.Router()
const UsersRouter = require('../routes/user')
const OrderRouter = require('../routes/order')
const MidtransRouter = require('../routes/midtrans')
const NewsRouter = require('./news')
const ServiceRouter = require('./service')
const VoucherRouter = require('./voucher')

router
    .use('/users', UsersRouter)
    .use('/news', NewsRouter)
    .use('/order', OrderRouter)
    .use('/notif', MidtransRouter)
    .use('/service', ServiceRouter)
    .use('/voucher', VoucherRouter)

module.exports = router