const express = require("express");
const router = express.Router();
const {OrderController} = require("../controller/order")

router.post("/input", OrderController.insert)
router.get("/status/:status/:order_id", OrderController.putStatus)
router.get("/detail/:order_id", OrderController.orderDetail)
router.get("/detail-tracking/:id", OrderController.orderDetailTracking)
router.get("/on-going/:customer_id", OrderController.getOngoing)
router.post("/review/:order_id", OrderController.Review)

module.exports = router;