const express = require("express");
const router = express.Router();
const {UsersController} = require("../controller/user")

router.post("/register", UsersController.register)
router.get("/:id", UsersController.getProfileDetail)
router.get("/verification-phone/:phone", UsersController.verificationWeb)
router.post("/change-pw/:phone", UsersController.changePass)
router.get("/check-phone/:phone", UsersController.checkPhone)
router.get("/otp-check/:phone/:otp", UsersController.checkOTP)
router.post("/login-mongose", UsersController.login_mongo)


module.exports = router;