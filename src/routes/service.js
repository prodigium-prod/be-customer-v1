const express = require("express");
const router = express.Router();
const {serviceController} = require("../controller/service")

router.post("/input",serviceController.inputService)
router.post("/search",serviceController.searchService)
router.get("/all",serviceController.getAllService)
router.get("/additional/:category",serviceController.getAllAdditional)

module.exports = router