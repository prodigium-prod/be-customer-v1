const express = require("express");
const router = express.Router();
const {VoucherController} = require("../controller/voucher")

router.post("/claim",VoucherController.claimVoucherController)

module.exports = router