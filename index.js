const express = require('express')
const {response} = require("./src/middleware/common");
require("dotenv").config();
const {default: mongoose} = require('mongoose');
const bodyParser = require('body-parser')
const morgan = require("morgan")
const mainRouter = require("./src/routes/index")
const cors = require('cors')

const app = express()

mongoose.connect(`${process.env.DB_PASS === 'DB_URL' ? process.env.DB_URL : process.env.DB_URL_DEV }`, {
    useNewUrlParser: true, useUnifiedTopology: true,
}).then(() => {
    console.log('Connected to MongoDB!');
}).catch((error) => {
    console.error('Error connecting to MongoDB:', error);
});

app.use(morgan("dev"))

app.use(bodyParser.json())

// const corsOptions = {
//   origin: 'http://localhost:3000', // ganti dengan URL frontend Anda
//   optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
// }

app.use(cors())


app.use("/", mainRouter)
app.use("/image", express.static('./Image'))

app.get('/', (req, res) => {
    res.send(`
    <div>
      <h1>Server is running!</h1>
    </div>
  `);
});

app.all("*", (req, res, next) => {
    response(res, 404, false, null, "404 Not Found")
})

const port = process.env.PORT

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})